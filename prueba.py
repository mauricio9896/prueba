import firebase_admin
from firebase_admin import credentials
from firebase_admin import db


from pyfirmata import Arduino, util
from tkinter import *
from PIL import Image
from PIL import ImageTk
import time

canvas_width =  425
canvas_height = 475

sizex_circ=60
sizey_circ=60

cont=0
prom=0

placa = Arduino ('COM3')
it = util.Iterator(placa)
it.start()


sensor2= placa.get_pin('a:1:i')
sensor3= placa.get_pin('a:2:i')



led3 = placa.get_pin('d:9:p')
led2 = placa.get_pin('d:10:p')



time.sleep(0.5)
ventana = Tk()
ventana.geometry('450x500')
ventana.title("UI para sistemas de control")

# Fetch the service account key JSON file contents
cred = credentials.Certificate('secreto/llave.json')
# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://base-de-daos.firebaseio.com/'
})

draw = Canvas(ventana, width=canvas_width, height=canvas_height,bg="plum1")
draw.place(x = 10,y = 10)

b=Label(ventana,text=":")

img = Image.open("C:/Users/LENOVO/Desktop/herramientas/usa.png")
img = img.resize((200,135))
photoImg=  ImageTk.PhotoImage(img)
b.configure(image=photoImg)
b.place(x = 120,y = 300)

#VARIABLES
x=0
p1=0
p2=0
vec1=[]
vec2=[]

def update_label():
    
    global cont1,x
    global sensor2
    global cont2
    global sensor3
    
    cont1=sensor2.read()                 
    cont_indicador2['text']=str(cont1)
    cont2=sensor3.read()
    cont_indicador3['text']=str(cont2)
    
    if(x<10):
        vec1.append(cont1)
        vec2.append(cont2)
        x=x+1
    ventana.after(1000,update_label)
    
#############################################################################    

def entrada(input):
    funcion=0
    funcion = dato.get()
    dato.delete(0, END)
    
    if(funcion=='i'):
        pwm()
        
    if(funcion=='p'):
        promedio()
        
    if(funcion=='g'):
        base_datos()
        
    if(funcion=='k'):
        reporte()
        
#########################################################################
def pwm():
    global sensor2,sensor3
    cont1=sensor2.read()
    cont2=sensor3.read()
    
    led3.write(cont1)
    led2.write(cont2)
    
    ventana.after(1000,pwm)
    
def promedio():
    global p1,p2

    p1=sum(vec1)/len(vec1)
    print("El promedio de A1 es: ")
    print(p1)

    p2=sum(vec2)/len(vec2)
    print("El promedio de A1 es: ")
    print(p2)

    print(vec1)
    print(vec2)
    
def base_datos():
    
    global sensor2,sensor3
    cont=sensor2.read()
    cont1=sensor3.read()
    
    ref = db.reference("ADC 1")
    ref.update({
                'Valor: ': cont
         })
    
    ref = db.reference("ADC 2")
    ref.update({
                'Valor: ': cont1
         })
    
def reporte():
    global p1,p2

    p1=sum(vec1)/len(vec1)
    print("El promedio de A1 es: ")
    print(p1)
    

    p2=sum(vec2)/len(vec2)
    print("El promedio de A1 es: ")
    print(p2)
    
    
    with open('C:/Users/LENOVO/Desktop/herramientas/parcial_herramientas.txt', 'w') as editado:
        editado.write("los 10 ultimos datos tomados de A1 son : "+'\n')
        editado.write(str (vec1)+'\n')
        editado.write("El promedio A1 es: ")
        editado.write(str (p1)+'\n'+'\n')

        editado.write("los 10 ultimos datos tomados de A2 son : "+'\n')
        editado.write(str (vec2)+'\n')
        editado.write("El promedio A2 es: ")
        editado.write(str (p2))
        
    
##############################################################################################

titulo=Label(ventana,text="SISTEMA DE CONTROL ",font=("Rockwell Extra Bold",18),fg="black")
titulo.place(x=60,y=15)
titulo.configure(bg='pale turquoise')

titulo1=Label(ventana,text="MAURCIO BUITRAGO",font=("Rockwell Extra Bold",14),fg="black")
titulo1.place(x=100,y=40)
titulo1.configure(bg='pale turquoise')
###################################################################################################


cont_indicador2= Label(ventana, text='0',bg='cadet blue1', font=("Arial Bold", 15), fg="white", width=5)
cont_indicador2.place(x=120, y=120)

start_button2=Label(ventana,text="A1",bg="plum1")
start_button2.place(x=135, y=160)

cont_indicador3= Label(ventana, text='0',bg='cadet blue1', font=("Arial Bold", 15), fg="white", width=5)
cont_indicador3.place(x=290, y=120)

start_button3=Label(ventana,text="A2",bg="plum1")
start_button3.place(x=305, y=160)
###############################################################################################

cont_indicador4= Label(ventana, text=' ',bg='plum1', font=("Arial Bold", 15), fg="white", width=5)
cont_indicador4.place(x=20, y=260)

cont_indicador5= Label(ventana, text=' ',bg='plum1', font=("Arial Bold", 15), fg="white", width=5)
cont_indicador5.place(x=120, y=260)

cont_indicador6= Label(ventana, text=' ',bg='plum1', font=("Arial Bold", 15), fg="white", width=5)
cont_indicador6.place(x=290, y=260)
####################################################################################################
dato = Entry(ventana)# declaracion como una ventana de entrada(necesario ubicacion)
dato.place(x=200, y=220)# ubicacion 
dato.bind('<Return>',entrada)#enlaza los datos a la funcion que establece

ces= Label(ventana, text='FUNCIONALIDAD:  ',bg='plum1', font=("Arial Bold", 10), fg="black")
ces.place(x=80, y=220)

update_label()
ventana.mainloop()

